// swift-tools-version: 5.6

import PackageDescription

let package = Package(
    name: "TestMainWindow",
    defaultLocalization: "ru",
    platforms: [.macOS(.v11)],
    dependencies: [
        .package(url: "https://github.com/DimaRU/qlift", branch: "master"),
        .package(path: "../../../qlift/QliftUIC"),
    ],
    targets: [
        .executableTarget(
            name: "TestMainWindow",
            dependencies: [
                .product(name: "Qlift", package: "Qlift"),
            ],
            exclude: ["UI/resource"],
            resources: [
                .process("Resources/tracewayjetson.rcc"),
            ],
            plugins: [
                .plugin(name: "QliftUICL10nPlugin", package: "QliftUIC"),
            ]),
    ]
)
