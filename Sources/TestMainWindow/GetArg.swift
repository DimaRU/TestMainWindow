//
//  GetArg.swift
//  
//
//  Created by Dmitriy Borovikov on 22.10.2021.
//

import Foundation

func getNumericalArg(_ arg: String) -> Int? {
    guard
        let index = CommandLine.arguments.firstIndex(of: arg),
        let next = CommandLine.arguments[safe: index + 1]
    else { return nil }
    return Int(next)
}

func getStringArg(_ arg: String) -> String? {
    guard
        let index = CommandLine.arguments.firstIndex(of: arg)
    else { return nil }
    return CommandLine.arguments[safe: index + 1]
}

func getBoolArg(_ arg: String) -> Bool {
    return CommandLine.arguments.contains(arg)
}

extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
