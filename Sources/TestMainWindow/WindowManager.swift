//
//  File.swift
//  
//
//  Created by Dmitriy Borovikov on 13.11.2021.
//

import Foundation
import Qlift

final class WindowManager {
    let mainWindow: QMainWindow
    var currentCentralWidget: QWidget?
    static var shared = WindowManager()

    enum SelectableWindow {
        case login
        case selectOrder
        case aggregation
        case serialization
    }

    init() {
        mainWindow = QMainWindow(parent: nil, flags: .Widget)
        mainWindow.resize(width: 1280, height: 1024)
        mainWindow.maximumSize = QSize(width: 1280, height: 1024)
        if !getBoolArg("--no-fullscreen") {
            mainWindow.show()
        } else {
            mainWindow.showFullScreen()
        }
    }

    func show(window: SelectableWindow, arg: String? = nil) {
        let centralWidget: QWidget
        switch window {
        case .login:
            centralWidget = LoginWindow()
        case .selectOrder:
            guard currentCentralWidget as? SelectOrderWindow == nil else { return }
            centralWidget = SelectOrderWindow()
        case .aggregation:
            guard currentCentralWidget as? AggregationWindow == nil else { return }
            centralWidget = AggregationWindow()
        case .serialization:
            centralWidget = SerializeWindow(orderName: "124")
        }

        self.mainWindow.centralWidget = centralWidget

        QTimer.singleShot(msec: 10, timerType: .CoarseTimer) {
            self.currentCentralWidget = centralWidget
        }
    }
}
