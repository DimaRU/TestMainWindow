//
//  LoginWindow.swift
//  
//
//  Created by Dmitriy Borovikov on 14.11.2021.
//

import Foundation
import Qlift

final class LoginWindow: UI_LoginWindow {

    init() {
        super.init()
        labelCleanup()
        connectButtons()
    }
    
    deinit {
        print("Deinit:", String(describing: Self.self))
    }
    
    private func labelCleanup() {
        homeButton.enabled = false
        qrButton.enabled = false
        boxButton.enabled = false
        palettButton.enabled = false
        toolsButton.enabled = false
        settingsButton.enabled = false
        logoutButton.enabled = true

        sessionLabel.text = ""
        staffNameLabel.text = ""
        loginFrame.visible = true
    }

    private func connectButtons() {
        enterButton.connectClicked(target: self) { $0.enterButtonClick }
        passwordLineEdit.connectReturnPressed(target: self) { $0.enterButtonClick }
        logoutButton.connectClicked(target: QApplication.instance) { $0.quit }
    }

    var showFirst = true
    override func showEvent(event: QShowEvent) {
        print(#function, String(describing: Self.self))
        super.showEvent(event: event)
        guard showFirst else { return }

        showFirst = false
    }

    override func closeEvent(event: QCloseEvent) {
        print(#function)
    }

    private func enterButtonClick() {
        WindowManager.shared.show(window: .selectOrder)
   }

}

