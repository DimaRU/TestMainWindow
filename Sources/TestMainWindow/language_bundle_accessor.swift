////
///  language_bundle_accessor.swift
//

import Foundation

@inlinable
func QTLocalizedString(_ s: String, tableName: String?, comment: String) -> String {
    NSLocalizedString(s, tableName: tableName, bundle: Bundle.lang, comment: comment)
}

extension Bundle {
    @usableFromInline
    static let lang: Bundle = {
        let lang = Locale.current.languageCode ?? "en"
        guard
            let path = Bundle.module.path(forResource: lang, ofType: "lproj"),
            let bundle = Bundle(path: path)
        else {
            return Bundle.module
        }
        return bundle
    }()
}
