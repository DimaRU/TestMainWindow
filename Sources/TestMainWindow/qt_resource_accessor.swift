////
///  qt_resource_accessor.swift
//

import Qlift
import Foundation

extension Bundle {

    class func registerResource() {
        guard
            let rccFilename = Bundle.module.path(forResource: "tracewayjetson", ofType: "rcc"),
            QResource.registerResource(rccFilename: rccFilename)
        else {
            fatalError("Can't load resources")
        }
    }

}
