//
//  AggregationWindow.swift
//  
//
//  Created by Dmitriy Borovikov on 17.06.2021.
//

import Foundation
import Qlift


class AggregationWindow: UI_Aggregation {

    // MARK: Init
    init() {
        super.init()
        connectButtons()
        labelCleanup()
    }

    deinit {
        print("Deinit:", String(describing: Self.self))
    }

    private func connectButtons() {
        printButton.connectClicked(target: self) { $0.printButtonClick }
        verifyButton.connectClicked(target: self) { $0.verifyButtonClick }
        orderDetailsButton.connectClicked(target: self) { $0.orderDetailsButtonClick }

        completeButton.connectClicked(target: self) { $0.completeButtonClick }

//        logoutButton.connectClicked { [unowned self] in logoutButtonClick() }
//        homeButton.connectClicked{ [unowned self] in homeButtonClick() }
//        qrButton.connectToggled(to: self.qrButtonTooggle)
//        boxButton.connectToggled(to: self.boxButtonToggle)
//        palettButton.connectToggled(to: self.palettButtonToggle)
//        settingsButton.connectToggled(to: self.settingsButtonToggle)
//        toolsButton.connectToggled(to: self.toolsButtonToggle)
//        retryPrintButton.connectClicked(to: self.retryPrintButtonClick)
//        clearButton.connectClicked(to: self.clearButtonClick)
//        partialButton.connectClicked(to: self.partialButtonClick)
//        printTemplateButton.connectClicked(to: self.printTemplateButtonClick)
//        headerButtonsGroup.connectIdToggled(to: headerGroupToggle)

    }

    private func labelCleanup() {
        alertIconLabel.visible = false
        checkmarkLabel.enabled = false
        boxButton.checked = true
        logoutButton.visible = false
        printButton.enabled = false
        retryPrintButton.enabled = false
        clearButton.enabled = false
        partialButton.enabled = false
        verifyButton.enabled = false   

        sessionLabel.text = ""
        staffNameLabel.text = ""
        assetNameLabel.text = ""
        fillBoxLabel.text = ""
        progressBar.value = 0

        layCountLabel.text = "0"
        layMaxLabel.text = "0"
        itemMaxLabel.text = "0"
        itemCountLabel.text = "0"
        orderLabel.text = ""
        itemCountLabel.text = "0"
    }

    var showFirst = true
    override func showEvent(event: QShowEvent) {
        print(#function, String(describing: Self.self))
        super.showEvent(event: event)
        guard showFirst else { return }

        showFirst = false
    }

    override func closeEvent(event: QCloseEvent) {
        print(#function)
    }

    func orderDetailsButtonClick() {
    }


    private func completeButtonClick() {
        QTimer.singleShot(msec: 100, timerType: .CoarseTimer) {
            WindowManager.shared.show(window: .selectOrder)
        }
    }

    private func logoutButtonClick() {
        QApplication.instance.quit()
    }

    func printButtonClick() {
    }

    func verifyButtonClick() {
    }
}
