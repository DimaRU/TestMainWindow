//
//  SelectOrderWindow.swift
//  
//
//  Created by Dmitriy Borovikov on 17.06.2021.
//

import Foundation
import Qlift

final class SelectOrderWindow: UI_SelectOrder, ClickableQFrameDelegate {

    init() {
        super.init()
        labelCleanup()
        connectButtons()
        scrollArea.sizeAdjustPolicy = .AdjustToContents
        scrollArea.widgetResizable = true
    }

    deinit {
        print("Deinit:", String(describing: Self.self))
    }

    private func connectButtons() {
        logoutButton.connectClicked(target: self) { $0.logoutButtonClick }
        homeButton.connectClicked(target: self) { $0.homeButtonClick }
        boxButton.connectClicked(target: self) { $0.boxButtonClick }
        qrButton.connectClicked(target: self) { $0.qrButtonClick }
        settingsButton.connectClicked(target: self) { $0.settingsButtonClick }
        toolsButton.connectClicked(target: self) { $0.toolsButtonClick }

        selectButtonsGroup.connectIdToggled(target: self) { $0.selectButtonToggle }
    }

    private func labelCleanup() {
        homeButton.checked = true
        qrButton.enabled = true
        boxButton.enabled = true
        boxButton.checkable = false
        palettButton.enabled = false

        button1.checked = true

        sessionLabel.text = "Время сессии  0:00"
    }

    var showFirst = true
    override func showEvent(event: QShowEvent) {
        print(#function, String(describing: Self.self))
        super.showEvent(event: event)
        guard showFirst else { return }

        showFirst = false
    }

    override func closeEvent(event: QCloseEvent) {
    }

    func click(id: Int) {
        switch id {
        case 1: qrButtonClick()
        case 2: boxButtonClick()
        default:
            break
        }
    }
    

    func boxButtonClick() {
        WindowManager.shared.show(window: .aggregation)
    }

    func qrButtonClick() {
        WindowManager.shared.show(window: .serialization)
    }
    
    private func logoutButtonClick() {
        QApplication.instance.quit()
    }

    private func settingsButtonClick() {}
    func toolsButtonClick() {}

    private func homeButtonClick() {
    }
    
    private func selectButtonToggle(id: Int32, state: Bool) {
    }

}

