//
//  ClickableQFrame.swift
//  
//
//  Created by Dmitriy Borovikov on 19.01.2022.
//

import Foundation
import Qlift

protocol ClickableQFrameDelegate: AnyObject {
    func click(id: Int)
}

final class ClickableQFrame: QFrame {
    weak var delegate: ClickableQFrameDelegate? = nil
    var id: Int = 0
    
    override func mousePressEvent(event: QMouseEvent) {
        guard event.button.contains(.LeftButton) else { return }
        delegate?.click(id: id)
    }
}
