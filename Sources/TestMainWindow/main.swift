////
/// main.swift
//

import Foundation
import Qlift

func main() -> Int32 {
    print("TraceWayJetson start")

    let application = QApplication()

    Bundle.registerResource()

    application.setStyle("Windows")
    application.windowIcon = QIcon(fileName: ":/header/Box.svg")

    WindowManager.shared.show(window: .login)
    return application.exec()
}

#if DEBUG
setbuf(stdout, nil)
#endif
exit(main())
