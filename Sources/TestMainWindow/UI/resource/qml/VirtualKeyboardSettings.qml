import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.1
import QtQuick.VirtualKeyboard.Settings 2.15

Rectangle {
//  Component.onCompleted: VirtualKeyboardSettings.styleName = "retro"
    Component.onCompleted: VirtualKeyboardSettings.activeLocales = ["ru_RU", "en_US"]
}
