//
//  AggregationWindow.swift
//  
//
//  Created by Dmitriy Borovikov on 17.06.2021.
//

import Foundation
import Qlift

class SerializeWindow: UI_SerializeWindow {

    // MARK: Init
    init(orderName: String) {
        super.init()
        serialFrame.visible = false
        connectButtons()
        labelCleanup()
    }

    deinit {
        print("Deinit:", String(describing: Self.self))
    }

    private func connectButtons() {
        logoutButton.connectClicked(target: self) { $0.logoutButtonClick }
        homeButton.connectClicked(target: self) { $0.homeButtonClick }
        boxButton.connectClicked(target: self) { $0.boxButtonClick }
        serializeButton.connectClicked(target: self) { $0.serializeButtonClick }
    }

    private func labelCleanup() {
        boxButton.checked = true

        sessionLabel.text = ""
        staffNameLabel.text = ""
    }

    var showFirst = true
    override func showEvent(event: QShowEvent) {
        print(#function, String(describing: Self.self))
        super.showEvent(event: event)
        guard showFirst else { return }
        showFirst = false
    }

    override func closeEvent(event: QCloseEvent) {
        print(#function)
    }
    
    private func boxButtonClick() {
        WindowManager.shared.show(window: .aggregation)
    }

    private func homeButtonClick() {
        WindowManager.shared.show(window: .selectOrder)
    }

    private func logoutButtonClick() {
        WindowManager.shared.show(window: .selectOrder)
    }
    
    private func serializeButtonClick() {
    }

}
